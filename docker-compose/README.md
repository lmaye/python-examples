# docker-compose

#### 介绍
学习Docker Compose案例。

#### 软件架构
基于Docker容器发布Python Web应用。

#### Docker Compose 安装
Docker Compose 是 Docker 的独立产品，因此需要安装 Docker 之后在单独安装 Docker Compose(推荐使用方法一进行安装):

1. 方法一
```
    #下载
    sudo curl -L https://github.com/docker/compose/releases/download/1.20.0/docker-compose-`uname -s`-`uname -m` -o /usr/local/bin/docker-compose
    #安装
    chmod +x /usr/local/bin/docker-compose
    #查看版本
    docker-compose version
```
2. 方法二
```
    #安装pip
    yum -y install epel-release
    yum -y install python-pip
    #确认版本
    pip --version
    #更新pip
    pip install --upgrade pip
    #安装docker-compose
    pip install docker-compose 
    #查看版本
    docker-compose version
```
3. 安装补全工具
为了方便我们输入命令，也可以安装 Docker 的补全提示工具帮忙我们快速输入命令
```
    #安装
    yum install bash-completion
    
    #下载docker-compose脚本
    curl -L https://raw.githubusercontent.com/docker/compose/$(docker-compose version --short)/contrib/completion/bash/docker-compose > /etc/bash_completion.d/docker-compose
```

#### Docker Compose 常用命令
```
    #查看帮助
    docker-compose -h
    
    # -f  指定使用的 Compose 模板文件，默认为 docker-compose.yml，可以多次指定。
    docker-compose -f docker-compose.yml up -d 
    
    #启动所有容器，-d 将会在后台启动并运行所有的容器
    docker-compose up -d
    
    #停用移除所有容器以及网络相关
    docker-compose down
    
    #查看服务容器的输出
    docker-compose logs
    
    #列出项目中目前的所有容器
    docker-compose ps
    
    #构建（重新构建）项目中的服务容器。服务容器一旦构建后，将会带上一个标记名，例如对于 web 项目中的一个 db 容器，可能是 web_db。可以随时在项目目录下运行 docker-compose build 来重新构建服务
    docker-compose build
    
    #拉取服务依赖的镜像
    docker-compose pull
    
    #重启项目中的服务
    docker-compose restart
    
    #删除所有（停止状态的）服务容器。推荐先执行 docker-compose stop 命令来停止容器。
    docker-compose rm 
    
    #在指定服务上执行一个命令。
    docker-compose run ubuntu ping docker.com
    
    #设置指定服务运行的容器个数。通过 service=num 的参数来设置数量
    docker-compose scale web=3 db=2
    
    #启动已经存在的服务容器。
    docker-compose start
    
    #停止已经处于运行状态的容器，但不删除它。通过 docker-compose start 可以再次启动这些容器。
    docker-compose stop
```

#### 参与贡献

1. Fork 本仓库
2. 新建 Feat_xxx 分支
3. 提交代码
4. 新建 Pull Request


#### 码云特技

1. 使用 Readme\_XXX.md 来支持不同的语言，例如 Readme\_en.md, Readme\_zh.md
2. 码云官方博客 [blog.gitee.com](https://blog.gitee.com)
3. 你可以 [https://gitee.com/explore](https://gitee.com/explore) 这个地址来了解码云上的优秀开源项目
4. [GVP](https://gitee.com/gvp) 全称是码云最有价值开源项目，是码云综合评定出的优秀开源项目
5. 码云官方提供的使用手册 [https://gitee.com/help](https://gitee.com/help)
6. 码云封面人物是一档用来展示码云会员风采的栏目 [https://gitee.com/gitee-stars/](https://gitee.com/gitee-stars/)