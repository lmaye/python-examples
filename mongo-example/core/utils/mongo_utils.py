#!/usr/bin/env python3
# -*- coding: utf-8 -*-
# ----------------------------------------------------------
# -- Mongo
# --
# ****************************
# Author: lmay.Zhou
# Blog: www.lmaye.com
# Date: 2021/1/13 17:22
# Email lmay@lmaye.com
# ----------------------------------------------------------
from pymongo import MongoClient
from core import LOG


class Mongo(object):
    """
    Mongo
    """

    def __init__(self):
        """
        对象初始化
        """
        # Mongo
        self.connect = MongoClient(host="192.168.30.240", port=37017, username="root", password="password")
        LOG.info("[{}] 数据库连接成功 ...".format(self.connect.address))

    def query(self, db, tb, params, column):
        """
        查询记录

        :param db: 数据库
        :param tb: 表
        :param params: 查询参数
        :param column: 查询字段
        :return:
        """
        # 获取db
        test_db = self.connect[db]
        # 获取collection
        collection = test_db[tb]
        return collection.find(params, column)

    def query_page(self, db, tb, params, column, page_size=100, page_idx=1):
        """
        查询记录

        :param db: 数据库
        :param tb: 表
        :param params: 查询参数
        :param column: 查询字段
        :param page_size: 页数
        :param page_idx: 页码
        :return:
        """
        # 获取db
        test_db = self.connect[db]
        # 获取collection
        collection = test_db[tb]
        # return collection.aggregate([
        #     {
        #         '$lookup': {
        #             "from": "AmazonJPOrders2020",
        #             "localField": "AmazonOrderId",
        #             "foreignField": "AmazonOrderId",
        #             "as": "order"
        #         }
        #     },
        #     {
        #         '$project': {
        #             'order.PurchaseDate': True, "order.OrderType": True, "order.SellerOrderId": True,
        #             'order.PaymentMethod': True, "order.OrderStatus": True, "order.SalesChannel": True,
        #             "AmazonOrderId": True, "ConditionSubtypeId": True, "QuantityShipped": True,
        #             "IsTransparency": True, "OrderItemId": True, "SellerSKU": True, "ASIN": True, "IsGift": True,
        #             "ConditionId": True, "Title": True, "QuantityOrdered": True, "PromotionDiscountTax": True,
        #             "ItemTax": True, "ItemPrice": True, "ProductInfo": True, "PromotionIds": True,
        #             "PromotionDiscount": True
        #         }
        #     }
        # ])
        return collection.find(params, column).limit(page_size).skip(page_size * (page_idx - 1))
