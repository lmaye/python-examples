#!/usr/bin/env python3
# -*- coding: utf-8 -*-
# ----------------------------------------------------------
# -- This"s System Enum
# --
# ****************************
# Author: lmay.Zhou
# Blog: www.lmaye.com
# Date: 2021年1月12日 18:14:02
# ----------------------------------------------------------
import math
import threading

from constant.sys_enum import SysEnum
from core import LOG
from utils import excel_utils
from utils.mongo_utils import Mongo

mongo = Mongo()
new_records = []
threads = []


def export_thread(records):
    for i in range(math.ceil(len(records) / 5000)):
        dt = records[i * 5000:(i + 1) * 5000]
        thread = threading.Thread(target=export_data_assembly, args=(i, dt))
        threads.append(thread)


def export_data_assembly(i, records):
    LOG.info("线程[{}]执行中...[{}] - [{}]".format(i, threading.currentThread().name, threading.currentThread().ident))
    column = {"AmazonOrderId": True, "PurchaseDate": True, "OrderType": True, "SellerOrderId": True,
              "PaymentMethod": True, "OrderStatus": True, "SalesChannel": True}
    for record in records:
        orders = mongo.query("oms", "AmazonUSOrders2020", {"AmazonOrderId": record["AmazonOrderId"]}, column)
        try:
            for key in column:
                try:
                    record[key] = orders[0][key]
                except KeyError:
                    record[key] = ""
                    continue
        except KeyError:
            continue
        new_records.append(record)
        LOG.info("线程名称[{}], 行数[{}]".format(threading.currentThread().name, len(new_records)))


if __name__ == "__main__":
    # 读取excel文件
    path = SysEnum.RESOURCES_PATH.value + SysEnum.SEPARATOR.value
    # 根据提供的Excel导数据
    # records = excel_utils.read_excel(path + "CA.xlsx", 0)
    # order_ids = []
    # for it in records:
    #     order_ids.append(it["order_id"])
    # column = {"AmazonOrderId": True, "PurchaseDate": True, "BuyerEmail": True, "OrderStatus": True, "ShippingAddress": True}
    # excel_handler.export_data(path + "Amazon.xls", order_ids, column)
    # 导出全部数据
    # column = {"AmazonOrderId": True, "ConditionSubtypeId": True, "QuantityShipped": True, "IsTransparency": True,
    #           "OrderItemId": True, "SellerSKU": True, "ASIN": True, "IsGift": True, "ConditionId": True, "Title": True,
    #           "QuantityOrdered": True, "PromotionDiscountTax": True, "ItemTax": True, "ItemPrice": True,
    #           "ProductInfo": True, "PromotionIds": True, "PromotionDiscount": True}
    # excel_handler.export_data3(path + "AmazonCAOrder_All.xls", db="oms", tb="AmazonCAOrderItems_All", column=column)
    # column = {"AmazonOrderId": True, "ConditionSubtypeId": True, "QuantityShipped": True, "IsTransparency": True,
    #           "OrderItemId": True, "SellerSKU": True, "ASIN": True, "IsGift": True, "ConditionId": True, "Title": True,
    #           "QuantityOrdered": True, "PromotionDiscountTax": True, "ItemTax": True, "ItemPrice": True,
    #           "ProductInfo": True, "PromotionIds": True, "PromotionDiscount": True}
    # excel_handler.export_data3(path + "AmazonJPOrder_All.xls", db="oms", tb="AmazonJPOrderItems2020", column=column)
    # 根据提供的Excel导数据
    # order_ids = []
    # excel_file = path + "AmazonUSOrder_All.xls"
    # records = excel_utils.read_excel(excel_file, 0)
    # for it in records:
    #     order_ids.append(it["AmazonOrderId"])
    # column = {"AmazonOrderId": True, "PurchaseDate": True, "OrderType": True, "SellerOrderId": True,
    #           "PaymentMethod": True, "OrderStatus": True, "SalesChannel": True}
    # excel_handler.export_data4(excel_file, db="oms", tb="AmazonUSOrders2020", order_ids=order_ids, column=column)

    # 多线程处理
    excel_file = path + "AmazonUSOrder_All.xls"
    data = excel_utils.read_excel(excel_file, 0)
    export_thread(data)
    for t in threads:
        t.start()
    for t in threads:
        t.join()
    LOG.info("处理完成[{}]".format(len(new_records)))
    excel_utils.write_excel_append(path + "test.xls", "Sheet1", new_records, sheet=0)
