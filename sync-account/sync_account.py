#!/usr/bin/env python3
# -*- coding: utf-8 -*-
# ----------------------------------------------------------
# -- 
# --
# ****************************
# Author: lmay.Zhou
# Blog: www.lmaye.com
# Date: 2021/1/14 13:40
# Email lmay@lmaye.com
# ----------------------------------------------------------
from handler import data_handler


if __name__ == "__main__":
    data_handler.sync_account()
