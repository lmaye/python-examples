#!/usr/bin/env python3
# -*- coding: utf-8 -*-
# ----------------------------------------------------------
# -- RSA Utils
# --
# ****************************
# Author: lmay.Zhou
# Blog: www.lmaye.com
# Date: 2021/1/14 14:45
# Email lmay@lmaye.com
# ----------------------------------------------------------
import base64

from Crypto.Cipher import PKCS1_v1_5
from Crypto.PublicKey import RSA

from constant.sys_enum import SysEnum


def encrypt(data):
    """
    RSA加密

    :param data: 加密数据
    :return: 密文
    """
    path = SysEnum.RESOURCES_PATH.value + SysEnum.SEPARATOR.value
    with open(path + "public.key", "rb") as f:
        pub_key = RSA.importKey(f.read())
    cipher = PKCS1_v1_5.new(pub_key)
    text = base64.b64encode(cipher.encrypt(str(data).encode()))
    return text.decode('utf8')
